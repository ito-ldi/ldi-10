/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package registros;

/**
 *
 * @author Luis
 */
public class Registros{
    private String[] regisAX;
    private String[] regisBX;
    private String[] regisCX;
    private String[] regisDX;
    
    private static final int bitSuperiorInicio = 1;
    private static final int bitSuperiorfin    = 8;
    private static final int bitInferiorInicio = 9;
    private static final int bitInferiorfin    = 16;
    private static final int bitInicio         = 1;
    private static final int bitFinal          = 16;
    
    public Registros(){
        regisAX = new String[17];
        regisBX = new String[17];
        regisCX = new String[17];
        regisDX = new String[17];
        inicializarRegistros();
    }
    public void inicializarRegistros(){
           regisAX[0] = "AX";
           regisBX[0] = "BX";
           regisCX[0] = "CX";
           regisDX[0] = "DX";
        for(int x = 1; x < regisAX.length; x++){
           regisAX[x] = "0";
           regisBX[x] = "0";
           regisCX[x] = "0";
           regisDX[x] = "0";
        }
    }
    
    public void viusalizarEstadoRegistro(String [] registro){        
        for(int x = 0; x < registro.length; x++){                          
                System.out.print(registro[x]+" ");  
        }
        System.out.println();
    }
    public void visualizarTodosLosRegistros(){
        String AX = "";
        String BX = "";
        String CX = "";
        String DX = "";
        for(int x = 0; x < regisAX.length; x++){                          
                AX += regisAX[x]+" ";
                BX += regisBX[x]+" ";
                CX += regisCX[x]+" ";
                DX += regisDX[x]+" ";
        }
        System.out.print(AX+"\n"+BX+"\n"+CX+"\n"+DX+"\n");  

    }
    public void guardar(String nomRegistro, String datoAGuardar){        
        String lon = convercionDecimalABinario(datoAGuardar);
        if(nomRegistro.equals("AX") && lon.length() <= 16){
            guardarEnRegistrosCompleto(regisAX, lon);
        }else if(nomRegistro.equals("BX") && lon.length() <= 16){
             guardarEnRegistrosCompleto(regisBX, lon);
        }else if(nomRegistro.equals("CX") && lon.length() <= 16){
             guardarEnRegistrosCompleto(regisCX, lon);
        }else if(nomRegistro.equals("DX") && lon.length() <= 16){
             guardarEnRegistroSuperior(regisDX, lon);
        }else if(nomRegistro.equals("AH") && lon.length() <= 8){
            guardarEnRegistroSuperior(regisAX, lon);
        }else if(nomRegistro.equals("BH") && lon.length() <= 8){
             guardarEnRegistroSuperior(regisBX, lon);
        }else if(nomRegistro.equals("CH") && lon.length() <= 8){
             guardarEnRegistroSuperior(regisCX, lon);
        }else if(nomRegistro.equals("DH") && lon.length() <= 8){
             guardarEnRegistroSuperior(regisDX, lon);
        }else if(nomRegistro.equals("AL") && lon.length() <= 8){
            guardarEnRegistroInferior(regisAX, lon);
        }else if(nomRegistro.equals("BL") && lon.length() <= 8){
             guardarEnRegistroInferior(regisBX, lon);
        }else if(nomRegistro.equals("CL") && lon.length() <= 8){
             guardarEnRegistroInferior(regisCX, lon);
        }else if(nomRegistro.equals("DL") && lon.length() <= 8){
             guardarEnRegistroInferior(regisDX, lon);
        }
    }
    
    public void sumar(String nomRegistro, String datoASumar){
         String lon = convercionDecimalABinario(datoASumar);
         String tmpValor = "";
         String suma = "";
        if(nomRegistro.equals("AX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisAX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisAX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("BX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisBX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisBX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("CX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisCX);         
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));            
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisCX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("DX") && lon.length() <= 16){
            tmpValor = valorDeRegistroCompleto(regisDX);
           suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 16){
                guardarEnRegistrosCompleto(regisDX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("AH") && lon.length() <= 8){
            tmpValor = valorDeRegistroCompleto(regisAX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisAX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("BH") && lon.length() <= 8){
            tmpValor = valorDeRegistroCompleto(regisBX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisBX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
            
        }else if(nomRegistro.equals("CH") && lon.length() <= 8){
             tmpValor = valorDeRegistroCompleto(regisCX);         
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));            
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisCX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("DH") && lon.length() <= 8){
           tmpValor = valorDeRegistroCompleto(regisDX);
           suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 8){
                guardarEnRegistroSuperior(regisDX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("AL") && lon.length() <= 8){
            tmpValor = valorDeRegistroCompleto(regisAX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisAX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}            
        }else if(nomRegistro.equals("BL") && lon.length() <= 8){
            tmpValor = valorDeRegistroCompleto(regisBX);
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisBX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("CL") && lon.length() <= 8){
             tmpValor = valorDeRegistroCompleto(regisCX);         
            suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));            
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisCX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }else if(nomRegistro.equals("DL") && lon.length() <= 8){
             tmpValor = valorDeRegistroCompleto(regisDX);
           suma     = sumaDeBinarios(lon, tmpValor.substring(tmpValor.length()-8, tmpValor.length()));    
            if(suma.length() <= 8){
                guardarEnRegistroInferior(regisDX, suma);
            }else{System.out.println("El valor de la suma es mayor a la capacidad del registro");}
        }
    }
    public void guardarEnRegistroInferior(String[] nomRegistro, String longitud){        
        for(int x = bitInferiorInicio,n = 0,res = 1; x<= bitInferiorfin; x++){
            try{
                if(longitud.substring(longitud.length()-res,longitud.length()) != null){
               nomRegistro[bitInferiorfin -n] = longitud.substring(longitud.length()-res,longitud.length()-n);
               n++;
               res++;
            }
            }catch(Exception e){}
        }
    }
    
    public void guardarEnRegistroSuperior(String[] nomRegistro, String longitud){
        for(int x = bitSuperiorInicio ,n =0; x<= bitSuperiorfin; x++){
            try{
                if(longitud.substring(longitud.length()-x, longitud.length()) != null){
                    nomRegistro[bitSuperiorfin-(x-1)] = longitud.substring(longitud.length()-x, longitud.length()-n);
                    n++;
                }    
            }catch(Exception ee){}            
            
        }
    }
    
    public void guardarEnRegistrosCompleto(String[] nombreRegistro, String longitud){
        for(int x = bitInicio,n=0; x<= bitFinal; x++){
            try{
                if(longitud.substring(longitud.length()-x,longitud.length()) != null){
               nombreRegistro[bitFinal-(x-1)] = longitud.substring(longitud.length()-x,longitud.length()-n);
               n++;              
              }
            }catch(Exception e){}
        }
    }

    public String valorDeRegistroCompleto(String[] nombreRegistro){
        String numBinario = "";
        for(int x = bitInicio,n=0; x<= bitFinal; x++){
            numBinario += nombreRegistro[x];
        }
        return numBinario;
    }
    public String valorDeRegistroSuperior(String[] nomRegistro, String longitud){
        String numBinario = "";
          for(int x = bitSuperiorInicio ,n =0; x<= bitSuperiorfin; x++){
            numBinario += nomRegistro[x];
        }
        System.out.println("num Binario"+numBinario);   
       return numBinario;   
    }
    
    public String valorDeRegistroInferior(String[] nomRegistro, String longitud){
        String numBinario = "";
        for(int x = bitInferiorInicio,n = 0,res = 1; x<= bitInferiorfin; x++){
            nomRegistro[x] += numBinario;
        }
        return numBinario;
    }
    public String convercionDecimalABinario(String numDecimal){
            String resultado = "";
            int decimal = Integer.parseInt(numDecimal);
            String[] numBinario = new String[100];
            int residuo;
            int x = 0;
            do{
                residuo = decimal % 2;                
                          decimal = decimal /2;                
                    numBinario[x] = residuo+"";
                    x++;
                                  
              }while(decimal > 0);               
             for(int y = numBinario.length-1; y >= 0; y--){            
                if(!(numBinario[y] == null)){
                  resultado += numBinario[y];
                }
        }     
        return resultado;
    }
    
    public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado;
    }
    
    
    public String[] getRegisAX() {
        return regisAX;
    }
    public String[] getRegisBX() {
        return regisBX;
    }
    public String[] getRegisCX() {
        return regisCX;
    }
    public String[] getRegisDX() {
        return regisDX;
    } 
    
    public static void main(String[] args){
        Registros re = new Registros();
       
        System.out.println("visualizar solo un registtro");
        re.viusalizarEstadoRegistro(re.getRegisAX());
        
        System.out.println("visualizar todos los registro");      
        re.visualizarTodosLosRegistros();
        
        System.out.println("guardar 15 en el registro superior AH"); 
        re.guardar("AH", "15");
        re.viusalizarEstadoRegistro(re.getRegisAX());
        System.out.println("guardar  255 en el registro inferior BL");  
        re.guardar("BL", "255");
        re.viusalizarEstadoRegistro(re.getRegisBX());
   
        System.out.println("guardar 35 en el registro CX"); 
        re.guardar("CX", "35");
        re.viusalizarEstadoRegistro(re.getRegisCX());
         System.out.println("visualizar todos los registros para ver los cambios");         
        re.visualizarTodosLosRegistros();
        
    //    re.viusalizarEstadoRegistro(re.getRegisCX());
        System.out.println("Sumar 10 en CX");
        re.sumar("CX", "10");
        re.viusalizarEstadoRegistro(re.getRegisCX());
        
         System.out.println("Sumar 50 en BX");
        re.sumar("DX", "50");
        re.viusalizarEstadoRegistro(re.getRegisDX());
        
         System.out.println("Sumar 33 en DX");
        re.sumar("DX", "33");
        re.viusalizarEstadoRegistro(re.getRegisDX());
        
    }
}
